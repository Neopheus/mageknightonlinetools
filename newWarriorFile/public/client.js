var socket;
var warriorFile = {};

$(document).ready(function(){

	socket = io.connect();
	socket.on("connect", function(){
		socket.emit("connected");
	});

	$(".marker").last().val("green");
	$(".marker").last().css("border-color", ABILITYCOLOR["green"].background);
	$(".marker").last().css("border-width", "2px");

	$("#edition, #name, #id").change(function(){
		socket.emit("alreadySaved", getID(), $("#name").val());
	});

	$("#subfaction").empty();
	var selectedFaction = $("#faction").val();
	for(var subfaction in FACTIONS[selectedFaction].SUBFACTIONS){
		$("#subfaction").append(
			$("<option value='"+subfaction+"'></option>").append(
				FACTIONS[selectedFaction].SUBFACTIONS[subfaction]
			)
		);
	}

	$("#rank").change(function(){
		if($(this).val()=="Unique"){
			$("input[name=equippable][value='false']").prop("checked",false);
			$("input[name=equippable][value='true']").prop("checked",true);
		} else {
			$("input[name=equippable][value='false']").prop("checked",true);
			$("input[name=equippable][value='true']").prop("checked",false);
		}
	});

	$("#faction").change(function(){
		updateSubfaction($(this).val());
	});

	$("#states").on("change", "td select[class!=marker]", function(){
		var value = $(this).val();
		var color = (value==="none") ? value : value.substring(1, value.length-1);
		color = ABILITYCOLOR[color].background;
		$(this).css("border-color", color);
		if(value[0]==="("){
			$(this).css("border-radius", "5px");
		} else {
			$(this).css("border-radius", "0px");
		}
		if(value=="none"){
			$(this).css("border-width", "1px");
		} else {
			$(this).css("border-width", "2px");
		}
	}).on("change", ".marker", function(){
		var value = $(this).val();
		var color = ABILITYCOLOR[value].background;
		$(this).css("border-color", color);
		if(value=="none"){
			$(this).css("border-width", "1px");
		} else {
			$(this).css("border-width", "2px");
		}
	});


	$("#addState").click(function(){
		$(".state").last().clone().appendTo("#states");
		$(".counter").last().html($(".state").length-1);
		$(".marker").last().val("none");
		$(".marker").last().css("border-color", ABILITYCOLOR["none"].background);
		$(".marker").last().css("border-width", "1px");
		var scrollTo = window.scrollY+$(".state").last().height();
		$("html, body").animate({scrollTop: scrollTo}, 250);
	});

	$("#removeState").click(function(){
		if($("#states .state").length>1){
			var scrollTo = window.scrollY-$(".state").last().height();
			$("html, body").animate({scrollTop: scrollTo}, 250).promise().then(function(){
				$(".state").last().remove();
			});
		}
	});

	$("#showFile, #saveFile").click(function(){
		//general attributes
		warriorFile["id"] = getID();
		warriorFile["name"] = $("#name").val();
		warriorFile["costs"] = parseInt($("#costs").val(), 10);
		var rank = $("#rank").val();
		warriorFile["rank"] = (rank=="Unique") ? rank : parseInt(rank, 10);
		warriorFile["faction"] = FACTIONS[$("#faction").val()].NAME;
		warriorFile["subfaction"] = FACTIONS[$("#faction").val()].SUBFACTIONS[$("#subfaction").val()];
		warriorFile["frontarc"] = parseInt($("#frontarc").val(), 10);
		warriorFile["reararc"] = parseInt($("#reararc").val(), 10);
		warriorFile["equippable"] = ($("input[name=equippable]:checked").val())=="true"?true:false;

		//fight attributes
		warriorFile["movementtype"] = $("#movementtype").val();
		warriorFile["attacktype"] = $("#attacktype").val();
		warriorFile["attackbonus"] = parseInt($("#attackbonus").val(), 10);
		warriorFile["defensetype"] = $("#defensetype").val();
		warriorFile["damagetype"] = $("#damagetype").val();
		warriorFile["range"] = parseInt($("#range").val(), 10);
		warriorFile["rangeattacks"] = parseInt($("#rangeattacks").val(), 10);
		warriorFile["rangedamage"] = parseInt($("#rangedamage").val(), 10);

		//lifestates
		warriorFile["life"] = $("#states tr").length;
		warriorFile["state"] = {};
		$(".state").each(function(){
			var stateID = $(this).children().first().html();
			var state = $(this).children().last();
			var boni1, boni2;
			warriorFile["state"][stateID] = {};
			warriorFile["state"][stateID]["marker"] = state.find(".marker").val();
			for(var value in VALUES){
				value = VALUES[value];
				if(value=="movement"){
					warriorFile["state"][stateID][value+"range"] = parseInt(state.find("."+value).val(), 10);
				} else {
					warriorFile["state"][stateID][value] = parseInt(state.find("."+value).val(), 10);
				}
				warriorFile["state"][stateID][value+"boni"] = [];
				bonus1 = state.find("."+value+"bonus1").val();
				bonus2 = state.find("."+value+"bonus2").val();
				if(bonus1!="none"){
					warriorFile["state"][stateID][value+"boni"][0] = bonus1;
					if(bonus2!="none"){
						warriorFile["state"][stateID][value+"boni"][1] = bonus2;
					}
				}
			}
		});

		//output
		if($(this)[0].id==="showFile"){
			output(warriorFile);
			$("#saveFile").prop("disabled", false);
		} else if($(this)[0].id==="saveFile") {
			confirm("Realy save file?",
				function(){
					socket.emit("newWarrior", warriorFile);
				}
			);
		}
	});

	$("#resetFile").click(function(){
		confirm("Realy reset form?",
			function(){
				resetForm();
				$("html, body").animate({scrollTop: 0}, 500);
			}
		);
	});

	// #############################
	// ### remote eventlisteners ###
	// #############################

	socket.on("msg", function(msg){
		alert(msg);
	});

	socket.on("warriorExists", function(){
		alert("CAUTION: This warrior is already in database!");
	});

});

function updateSubfaction(selectedFaction){
	$("#subfaction").empty();
	for(var subfaction in FACTIONS[selectedFaction].SUBFACTIONS){
		$("#subfaction").append(
			$("<option value='"+subfaction+"'></option>").append(
				FACTIONS[selectedFaction].SUBFACTIONS[subfaction]
			)
		);
	}
}

function output(warrior){
	$("#file").remove();
	var file = $("<form id='file'></form>");
	var fieldset = $("<fieldset></fieldset>").append($("<legend></legend>").append("Output"));
	var filename = getFilename(warrior.id, warrior.name);
	fieldset.append($("<input type='text' id='filename' size='75'>").val(filename));
	fieldset.append($("<br><br>"));
	var filecontent = JSON.stringify(warrior, null, 2);
	fieldset.append($("<pre id='output'></pre>").html(filecontent));
	var downloadFile = $("<a></a>").append("Save");
	downloadFile.attr("id", "download");
	downloadFile.attr("href", "data:application/xml;charset=utf-8,"+filecontent);
	downloadFile.attr("download", filename);
	fieldset.append(downloadFile);
	file.append(fieldset);
	$("#grid").append(file);
}

function resetForm(){
	$("#saveFile").prop("disabled", true);
	$("#file").remove();

	//general attributes
	$("#edition").val("basic");
	$("#id").val(1);
	$("#name").val("");
	$("#costs").val(1);
	$("#rank").val(1);
	$("#faction").val("07");
	updateSubfaction("00");
	$("#subfaction").val("00");
	$("#frontarc").val(3);
	$("#reararc").val(3);
	$("input[name=equippable][value='false']").prop("checked",true);
	$("input[name=equippable][value='true']").prop("checked",false);

	//fight attributes
	$("#movementtype").val("shoe");
	$("#attacktype").val("sword");
	$("#attackbonus").val(0);
	$("#defensetype").val("normal");
	$("#damagetype").val("normal");
	$("#range").val(0);
	$("#rangeattacks").val(1);
	$("#rangedamage").val(0);

	//lifestates
	while($("#states tr").length>1){
		$("#states tr").last().remove();
	}
	$("#states input").val(0);
	$("#states select[class!=marker]").val("none");
	$("#states select[class!=marker]").css("border-color", "#888");
	$("#states select[class!=marker]").css("border-radius", "0px");
	$("#states select[class!=marker]").css("border-width", "1px");
}

function getID(){
	var id = $("#id").val()+"";
	while(id.length<3) id="0"+id;
	var fullId = $("#edition").val() + "_" + id;
	return fullId;
}

function getFilename(id, name){
	return id+" - "+name+".MKOC";
}

var FACTIONS = {
	"00": {
		"NAME": "Apocalypse",
		"SUBFACTIONS": {
			"00": "none",
			"01": "Minions of Apocalypse"
		}
	},
	"01": {
		"NAME": "Atlantean Empire",
		"SUBFACTIONS": {
			"00": "none",
			"01": "Golemkore",
			"02": "Imperial Legion",
			"03": "Delphana"
		}
	},
	"02": {
		"NAME": "Black Powder Revulutionaries",
		"SUBFACTIONS": {
			"00": "none",
			"01": "Bloody Thorns",
			"02": "Northlanders",
			"03": "Forgemasters"
		}
	},
	"03": {
		"NAME": "Dark Crusaders",
		"SUBFACTIONS": {
			"00": "none",
			"01": "Deathspeakers",
			"02": "Order of Vladd",
			"03": "Blood Cultists"
		}
	},
	"04": {
		"NAME": "Draconum",
		"SUBFACTIONS": {
			"00": "none",
			"01": "Dragon Mystics"
		}
	},
	"05": {
		"NAME": "Elemental Freeholds",
		"SUBFACTIONS": {
			"00": "none",
			"01": "Wylden Host",
			"02": "Storm Druids"
		}
	},
	"06": {
		"NAME": "Elven Lords",
		"SUBFACTIONS": {
			"00": "none",
			"01": "Free Armies",
			"02": "Temple Masters",
			"03": "Order of Sorcery"
		}
	},
	"07": {
		"NAME": "Mage Spawn",
		"SUBFACTIONS": {
			"00": "none",
			"01": "Order of the Ninth Circle"
		}
	},
	"08": {
		"NAME": "Orc Kahns",
		"SUBFACTIONS": {
			"00": "none",
			"01": "Broken Tusk",
			"02": "Shadow Khans",
			"03": "Chaos Shamans"
		}
	},
	"09": {
		"NAME": "Shyft",
		"SUBFACTIONS": {
			"00": "none"
		}
	},
	"10": {
		"NAME": "Solonavi",
		"SUBFACTIONS": {
			"00": "none",
			"01": "Oracles of Rokos"
		}
	}
};

var VALUES = {
	0: "movement",
	1: "attack",
	2: "defence",
	3: "damage"
};

var ABILITYCOLOR = {
	red: {
		background: "#FF0000",
		text: "#FFFFFF"
   },
	green: {
		background: "#00FF00",
		text: "#000000"
   },
	blue: {
		background: "#0000FF",
		text: "#FFFFFF"
   },
	purple: {
		background: "#CC0066",
		text: "#FFFFFF"
   },
	yellow: {
		background: "#FFFF00",
		text: "#000000"
   },
	orange: {
		background: "#FF6600",
		text: "#000000"
   },
	brown: {
		background: "#CC6600",
		text: "#FFFFFF"
   },
	gray: {
		background: "#a6a6a6",
		text: "#FFFFFF"
   },
	black: {
		background: "#000000",
		text: "#FFFFFF"
   },
	none: {
		background: "#888888",
		text: "#000000"
   }
};
