var express = require("express");
var app = express();
var server = require("http").createServer(app);
var io = require("socket.io").listen(server);
var config = require("./config.json");
var fs = require("fs");
var wl = require("./warriorsList.js");

server.listen(config.port);
app.use(express.static(__dirname + "/public"));
app.get("/", function(req, res){
	res.sendFile(__dirname + "/public/index.html");
});

var DEBUG = false;

//var dummy = {id:"basic_001", name:"Shocktrooper"};
//console.log(wl.warriors["basic"]["001"].name);
//console.log(wl.isExistingWarrior(dummy));

//var warriorList = JSON.parse(fs.readFileSync("./warriorsList.json", "utf8"));

wl.connectLists();
fs.writeFileSync("./warriors.json", JSON.stringify(wl.warriors, null, 4), "utf8");

io.sockets.on("connection", function(socket){

	socket.on("connected", function(){
		console.log("New User on Site.");
	});

	socket.on("alreadySaved", function(id, name){
		var path = "./savedWarriors/"+id+" - "+name+".MKOC"
		if(exists(path)){
			message("CAUTION: '"+id+" - "+name+"' is already in database!");
		}
	});

	socket.on("newWarrior", function(warrior){
		var validation = validateWarrior(warrior);
		if(validation==="VALID"){
			saveWarrior(warrior);
		} else {
			message("Warrior is not valid: "+validation);
		}
	});

	function validateWarrior(warrior){
		if(DEBUG) console.log("### validationtest ###");

		var edition = warrior.id.substring(0,warrior.id.length-4);
		var id = parseInt(warrior.id.substring(warrior.id.length-3, warrior.id.length),10);

		if(DEBUG) console.log("Edition: "+edition);
		if(EDITIONS.indexOf(edition)===-1) return "No valid Edition.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("ID: "+id);
		if(id<=0 || id>=1000) return "Not a valid ID number.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Name: "+warrior.name);
		if(typeof warrior.name != "string") return "Not a valid String as name.";
		if(warrior.name==="") return "Empty name.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Realy existing warrior? "+wl.isExistingWarrior(warrior));
		if(!wl.isExistingWarrior(warrior)) return "This warrior is not existing (Edition/ID/Name combination).";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Costs: "+warrior.costs);
		if(typeof warrior.costs != "number") return "Costs are not a valid Integer.";
		if(warrior.costs<=0) return "Costs are 0 or less.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Rank: "+warrior.rank);
		if(RANKS.indexOf(warrior.rank)===-1) return "This rank is not existing.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Faction: "+warrior.faction);
		if(!FACTIONS.hasOwnProperty(warrior.faction)) return "This faction is not existing.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Subfaction: "+warrior.subfaction);
		if(FACTIONS[warrior.faction].SUBFACTIONS.indexOf(warrior.subfaction)===-1) return "This subfaction is not existing.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Frontarc: "+warrior.frontarc);
		if(typeof warrior.frontarc != "number") return "Frontarc is not a valid Integer.";
		if(warrior.frontarc<=0 || warrior.frontarc>12) return "Frontarc is not between 0 and 12.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Reararc: "+warrior.reararc);
		if(typeof warrior.reararc != "number") return "Reararc is not a valid Integer.";
		if(warrior.reararc<=0 || warrior.reararc>12) return "Reararc is not between 0 and 12.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Arcs: "+(warrior.frontarc+warrior.reararc));
		if(warrior.frontarc+warrior.reararc > 12) return "The sum of front and reararc is higher than 12.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Equippable: "+warrior.equippable);
		if(typeof warrior.equippable != "boolean") return "'Equippable' can only be 'true' or 'false'.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Types:");
		for(var type in TYPES){
			if(DEBUG) console.log(type+": "+warrior[type]);
			if(TYPES[type].indexOf(warrior[type])===-1) return warrior[type]+" is not a valid type of "+type;
			if(DEBUG) console.log("checked");
		}
		if(DEBUG) console.log("---checked---");

		if(DEBUG) console.log("Range: "+warrior.range);
		if(typeof warrior.range != "number") return "Range is not a valid Integer.";
		if(warrior.range < 0) return "Range is smaller than 0.";
		if(DEBUG) console.log("---checked---");

		if(DEBUG) console.log("Rangeattacks: "+warrior.rangeattacks);
		if(typeof warrior.rangeattacks != "number") return "Rangeattacks is not a valid Integer.";
		if(warrior.rangeattacks < 1) return "Rangeattacks is smaller than 1.";
		if(DEBUG) console.log("---checked---");

		if(DEBUG) console.log("Rangedamage: "+warrior.rangedamage);
		if(typeof warrior.rangedamage != "number") return "Rangedamage is not a valid Integer.";
		if(warrior.rangedamage < 0) return "Rangedamage is smaller than 0.";
		if(DEBUG) console.log("---checked---");

		if(DEBUG) console.log("Attackbonus: "+warrior.attackbonus);
		if(typeof warrior.attackbonus != "number") return "Attackbonus is not a valid Integer.";
		if(warrior.attackbonus < 0) return "Attackbonus is smaller than 0.";
		if(DEBUG) console.log("---check---");

		if(DEBUG) console.log("Lifestates:");
		for(var state in warrior.state){
			if(DEBUG) console.log("State: "+state);
			var stateCounter = state;
			state = warrior["state"][state];

			if(DEBUG) console.log("Marker: "+state.marker);
			if(MARKERS.indexOf(state.marker)===-1) return "Marker of state "+stateCounter+" is not a valid marker.";
			if(DEBUG) console.log("checked");

			if(DEBUG) console.log("Movementrange: "+state.movementrange);
			if(typeof state.movementrange != "number") return "Movementrange is not a valid Integer.";
			if(state.movementrange < 0) return "Movementrange is smaller than 0.";
			if(DEBUG) console.log("checked");

			if(DEBUG) console.log("Movementboni:");
			for(var i=0; i<state.movementboni.length; i++){
				if(DEBUG) console.log("Boni: "+state.movementboni[i]);
				if(ABILITIYCOLORS.indexOf(state.movementboni[i])===-1) return "Movementability "+state.movementboni[i]+" in state "+stateCounter+" is not existing.";
				if(i===1 && state.movementboni[0]===state.movementboni[1]) return "Same Movementability in Slot 2 as in 1 in state "+stateCounter+".";
				if(DEBUG) console.log("checked");
			}

			if(DEBUG) console.log("Attack: "+state.attack);
			if(typeof state.attack != "number") return false;
			if(state.attack < 0) return false;
			if(DEBUG) console.log("checked");

			if(DEBUG) console.log("Attackboni:");
			for(var i=0; i<state.attackboni.length; i++){
				if(DEBUG) console.log("Boni: "+state.attackboni[i]);
				if(ABILITIYCOLORS.indexOf(state.attackboni[i])===-1) return "Attackability "+state.attackboni[i]+" in state "+stateCounter+" is not existing.";
				if(i===1 && state.attackboni[0]===state.attackboni[1]) return "Same Attackability in Slot 2 as in 1 in state "+stateCounter+".";
				if(DEBUG) console.log("checked");
			}

			if(DEBUG) console.log("Defence: "+state.defence);
			if(typeof state.defence != "number") return false;
			if(state.defence < 0) return false;
			if(DEBUG) console.log("checked");

			if(DEBUG) console.log("Defenceboni:");
			for(var i=0; i<state.defenceboni.length; i++){
				if(DEBUG) console.log("Boni: "+state.defenceboni[i]);
				if(ABILITIYCOLORS.indexOf(state.defenceboni[i])===-1) return "Defenceability "+state.defenceboni[i]+" in state "+stateCounter+" is not existing.";
				if(i===1 && state.defenceboni[0]===state.defenceboni[1]) return "Same Defenceability in Slot 2 as in 1 in state "+stateCounter+".";
				if(DEBUG) console.log("checked");
			}

			if(DEBUG) console.log("Damage: "+state.damage);
			if(typeof state.damage != "number") return false;
			if(state.damage < 0) return false;
			if(DEBUG) console.log("checked");

			if(DEBUG) console.log("Damageboni:");
			for(var i=0; i<state.damageboni.length; i++){
				if(DEBUG) console.log("Boni: "+state.damageboni[i]);
				if(ABILITIYCOLORS.indexOf(state.damageboni[i])===-1) return "Damageability "+state.damageboni[i]+" in state "+stateCounter+" is not existing.";
				if(i===1 && state.damageboni[0]===state.damageboni[1]) return "Same Damageability in Slot 2 as in 1 in state "+stateCounter+".";
				if(DEBUG) console.log("checked");
			}

		}
		if(DEBUG) console.log("---checked---");

		if(DEBUG) console.log("### valid ###");
		return "VALID";
	}

	function saveWarrior(warrior){
		var path = getPath(warrior);
		if(!exists(path)){
			fs.writeFileSync(path, JSON.stringify(warrior, null, 4), "utf8");
			message("Saved: '"+warrior.id+" - "+warrior.name+"'");
		} else {
			message("Not saved: '"+warrior.id+" - "+warrior.name+"' already in database.");
		}
	}

	function getID(warrior){
		return warrior.edition+"_"+warrior.id;
	}

	function getPath(warrior){
		return "./savedWarriors/"+warrior.id+" - "+warrior.name+".MKOC";
	}

	function exists(path){
		try{
			fs.accessSync(path, (fs.constants||fs).F_OK);
			return true;
		} catch(e){
			return false;
		}
	}

	function message(msg){
		console.log(msg);
		socket.emit("msg", msg);
	}

});

var EDITIONS = [
	"basic",
	"darkriders",
	"sorcery",
	"omens",
	"nexus"
]

var RANKS = [
	1,
	2,
	3,
	"Unique"
]

var FACTIONS = {
	"Apocalypse": {
		"SUBFACTIONS": [
			"none",
			"Minions of Apocalypse"
		]
	},
	"Atlantean Empire": {
		"SUBFACTIONS": [
			"none",
			"Golemkore",
			"Imperial Legion",
			"Delphana"
		]
	},
	"Black Powder Revulutionaries": {
		"SUBFACTIONS": [
			"none",
			"Bloody Thorns",
			"Northlanders",
			"Forgemasters"
		]
	},
	"Dark Crusaders": {
		"SUBFACTIONS": [
			"none",
			"Deathspeakers",
			"Order of Vladd",
			"Blood Cultists"
		]
	},
	"Draconum": {
		"SUBFACTIONS": [
			"none",
			"Dragon Mystics"
		]
	},
	"Elemental Freeholds": {
		"SUBFACTIONS": [
			"none",
			"Wylden Host",
			"Storm Druids"
		]
	},
	"Elven Lords": {
		"SUBFACTIONS": [
			"none",
			"Free Armies",
			"Temple Masters",
			"Order of Sorcery"
		]
	},
	"Mage Spawn": {
		"SUBFACTIONS": [
			"none",
			"Order of the Ninth Circle"
		]
	},
	"Orc Kahns": {
		"SUBFACTIONS": [
			"none",
			"Broken Tusk",
			"Shadow Khans",
			"Chaos Shamans"
		]
	},
	"Shyft": {
		"SUBFACTIONS": [
			"none"
		]
	},
	"Solonavi": {
		"SUBFACTIONS": [
			"none",
			"Oracles of Rokos"
		]
	}
};

var TYPES = {
	"movementtype": [
		"shoe",
		"horseshoe",
		"wing",
		"wave"
	],
	"attacktype": [
		"sword",
		"bow",
		"wand"
	],
	"defensetype": [
		"normal",
		"magic"
	],
	"damagetype": [
		"normal",
		"golem"
	]
};

var MARKERS = [
	"none",
	"green",
	"black"
];

var ABILITIYCOLORS = [
	"(red)",
	"[red]",
	"(green)",
	"[green]",
	"(blue)",
	"[blue]",
	"(purple)",
	"[purple]",
	"(yellow)",
	"[yellow]",
	"(orange)",
	"[orange]",
	"(brown)",
	"[brown]",
	"(gray)",
	"[gray]",
	"(black)",
	"[black]",
	"none"
];


//###################
//### END OF CODE ###
//###################

console.log("###################################################");
console.log("### Server is running on http://127.0.0.1:" + config.port + "/ ###");
console.log("###################################################");
