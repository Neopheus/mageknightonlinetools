   #################
   ### MKO Tools ###
   #################

Author: Bernhard Maier (mail@neopheus.de)
Platform: WebApps

About: 
Tools to improve work on Mage Knight Online. Implemented as separated Node.js Server.

Available tools:
 - newWarriorFile (tool for easy digitalization of MK miniatures)